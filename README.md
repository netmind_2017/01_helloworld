# GIT CONFIG #
 - git config --global user.name "Andrea Gelsomino"
 - git config --global user.email "andrea_gelsomino@hotmail.com"
 - git config --list (check)

# INIT PROJECT #
- git init 
- git add -A
- git status
- git commit -m "first comment"
- git remote add origin [REMOTE Project Link]
- git push -u origin master

#  COMMIT CAHNGES #

- git status 
- git add -A
- git commit -m "I've changed ......"
- git push

# UPDATE LOCAL VERSION #
- git pull

# CLONE PROJECT #
- git clone <YOUR BITBUCKET PROJECT>

# TO IGNORE FILE TO UPDATE #
 - Create a file .gitignore with: *.gif *.jpg *.ico *.txt
 - WINDOWS rename gitignore.txt in .gitignore  from MS-DOC COMMAND LINE execure ren gitignore.txt .gitignore
 - http://www.winelove.club/doc/git/#/16

# CHANGE REPOSITORY #
 - git remote set-url origin [ProjectLink].git
 - git remote -v


http://www.winelove.club/doc/git/#/9